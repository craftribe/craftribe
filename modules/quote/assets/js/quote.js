/*
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

(function($){
	var quoteForm = document.getElementById( 'quote-form' );

	new stepsForm( quoteForm, {
		onSubmit : function( form ) {
			// hide form
			classie.addClass( quoteForm.querySelector( '.quoteform-inner' ), 'hide' );

			/*
			 form.submit()
			 or
			 AJAX request (maybe show loading indicator while we don't have an answer..)
			 */

			// let's just simulate something...
			var messageEl = quoteForm.querySelector( '.final-message' );
			messageEl.innerHTML = 'Thank you! We\'ll be in touch.';
			classie.addClass( messageEl, 'show' );
		}
	} );
})(jQuery);