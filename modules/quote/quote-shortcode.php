<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

// Add quote Shortcode
function quote_output() {
	
	ob_start();
	
	?>
	<div class="md-modal md-effect-1" id="modal-1">
		<div class="md-content">
			<h3>Request a Quote</h3>
			<div>
				<form id="quote-form" class="quoteform" autocomplete="off">
					<div class="quoteform-inner">
						<ol class="questions">
							<li>
								<span><label for="q1">What is your full name?</label></span>
								<input id="q1" name="q1" type="text"/>
							</li>
							<li>
								<span><label for="q2">What is your email address?</label></span>
								<input id="q2" name="q2" type="text"/>
							</li>
							<li>
								<span><label for="q3">What is your phone number?</label></span>
								<input id="q3" name="q3" type="text"/>
							</li>
							<li>
								<span><label for="q4">In what town/city is your property?</label></span>
								<input id="q4" name="q4" type="text"/>
							</li>
							<li>
								<span><label for="q5">What is your postcode?</label></span>
								<input id="q5" name="q5" type="text"/>
							</li>
							<li>
								<span><label for="q6">When is the best time to call?</label></span>
								<input id="q6" name="q6" type="text"/>
							</li>
						</ol>
						<button class="submit" type="submit">Send answers</button>
						<div class="controls">
							<button class="next"></button>
							<div class="progress"></div>
                            <span class="number">
	                            <span class="number-current"></span>
	                            <span class="number-total"></span>
                            </span>
							<span class="error-message"></span>
						</div>
					</div>
					<span class="final-message"></span>
				</form>
			</div>
		</div>
	</div>
	<div class="md-overlay"></div>

	<script>
		// this is important for IEs
		var polyfilter_scriptpath = "<?= get_template_directory() . '/modules/quote/assets/js/';?>";
	</script>
	
	<?php
	
	$output = ob_get_clean();
	
	return $output;
	
}

add_shortcode( 'quote_form', 'quote_output' );
