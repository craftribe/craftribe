<?php

$quote_includes = [
	'/modules/quote/quote-shortcode.php'
];

foreach ( $quote_includes as $file ) {
	if ( ! $filepath = locate_template( $file ) ) {
		trigger_error( sprintf( __( 'Error locating %s for inclusion', 'framework' ), $file ), E_USER_ERROR );
	}
	
	require_once $filepath;
}
unset( $file, $filepath );

/**
 * Module assets
 */
function quote_assets() {
	wp_enqueue_script( 'framework/quote-modernizr', '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', null, null, false );
	wp_enqueue_script( 'framework/quote-classie', get_template_directory_uri() . '/modules/quote/assets/js/classie.js', null, null, true );
	wp_enqueue_script( 'framework/quote-modal', get_template_directory_uri() . '/modules/quote/assets/js/modal-effects.js', null, null, true );
	wp_enqueue_script( 'framework/quote-form', get_template_directory_uri() . '/modules/quote/assets/js/stepsForm.js', [ 'jquery' ], null, true );
	wp_enqueue_script( 'framework/quote', get_template_directory_uri() . '/modules/quote/assets/js/quote.js', [ 'jquery' ], null, true );
	wp_enqueue_script( 'framework/quote-modal', get_template_directory_uri() . '/modules/quote/assets/js/css-polyfilters.js', null, null, true );
	wp_enqueue_script( 'framework/quote-modal', get_template_directory_uri() . '/modules/quote/assets/js/css-parser.js', null, null, true );
}

add_action( 'wp_enqueue_scripts', 'quote_assets', 200 );
