<?php
//add_action( 'cmb2_admin_init', 'case_study_metabox' );

function case_study_metabox() {

	$prefix = 'case_study_';

	$case_study = new_cmb2_box( [
		                        'id'           => $prefix . 'metabox',
		                        'title'        => esc_html__( 'Repeating Slide Group', 'cmb2' ),
		                        'object_types' => [ 'page', ],
	                        ] );

}