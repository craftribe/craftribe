<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

// Add case-study Shortcode
function case_study_output() {
	
	$faq_count = 0;
	
	ob_start();
	
	?>
	
	<div class="case-study">

		<div class="case-study-container">
			<?php

			$slides = get_post_meta( get_the_ID(), 'case_study_case_study', true );

			foreach ( $slides as $slide ) :

				echo '<div><img src="' . $slide['image'] . '"></div>';

			endforeach;

			?>
		</div>

		<div class="overlay">
			<h2>Welcome to <?php bloginfo( 'name' ); ?></h2>
			<?= wpautop( get_post_meta( get_the_ID(), 'case_study_text', true ) ); ?>
		</div>

	</div>

	<?php

	// set output
	$output = ob_get_clean();
	
	// return output
	return $output;
	
}

add_shortcode( 'case_study', 'case_study_output' );
