<?php

$case_study_includes = [
	'/modules/case-study/case-study-cpt.php',
	'/modules/case-study/case-study-custom-fields.php',
	'/modules/case-study/case-study-shortcode.php'
];

foreach ($case_study_includes as $file) {
	if (!$filepath = locate_template($file)) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'framework'), $file), E_USER_ERROR);
	}
	
	require_once $filepath;
}
unset($file, $filepath);

