<?php

function case_study_init() {
	register_post_type( 'case_study', array(
		'labels'            => array(
			'name'                => __( 'Case Studies', 'cmb' ),
			'singular_name'       => __( 'Case Study', 'cmb' ),
			'all_items'           => __( 'All Case Studies', 'cmb' ),
			'new_item'            => __( 'New Case Study', 'cmb' ),
			'add_new'             => __( 'Add New', 'cmb' ),
			'add_new_item'        => __( 'Add New Case Study', 'cmb' ),
			'edit_item'           => __( 'Edit Case Study', 'cmb' ),
			'view_item'           => __( 'View Case Study', 'cmb' ),
			'search_items'        => __( 'Search Case Studies', 'cmb' ),
			'not_found'           => __( 'No Case Studies found', 'cmb' ),
			'not_found_in_trash'  => __( 'No Case Studies found in trash', 'cmb' ),
			'parent_item_colon'   => __( 'Parent Case Study', 'cmb' ),
			'menu_name'           => __( 'Case Studies', 'cmb' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-thumbs-up',
		'show_in_rest'      => true,
		'rest_base'         => 'case_study',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );
	
}
add_action( 'init', 'case_study_init' );

function case_study_updated_messages( $messages ) {
	global $post;
	
	$permalink = get_permalink( $post );
	
	$messages['case_study'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Case Study updated. <a target="_blank" href="%s">View Case Study</a>', 'cmb'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'cmb'),
		3 => __('Custom field deleted.', 'cmb'),
		4 => __('Case Study updated.', 'cmb'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Case Study restored to revision from %s', 'cmb'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Case Study published. <a href="%s">View Case Study</a>', 'cmb'), esc_url( $permalink ) ),
		7 => __('Case Study saved.', 'cmb'),
		8 => sprintf( __('Case Study submitted. <a target="_blank" href="%s">Preview Case Study</a>', 'cmb'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Case Study scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Case Study</a>', 'cmb'),
			// translators: Publish box date format, see http://php.net/date
			          date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Case Study draft updated. <a target="_blank" href="%s">Preview Case Study</a>', 'cmb'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);
	
	return $messages;
}
add_filter( 'post_updated_messages', 'case_study_updated_messages' );