<?php

$slider_includes = [
	'/modules/slider/slider-custom-fields.php',
	'/modules/slider/slider-shortcode.php'
];

foreach ($slider_includes as $file) {
	if (!$filepath = locate_template($file)) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'framework'), $file), E_USER_ERROR);
	}
	
	require_once $filepath;
}
unset($file, $filepath);

/**
 * Module assets
 */
function slider_assets() {

	wp_enqueue_script('framework/slider', get_template_directory_uri().'/modules/slider/assets/js/slider.js', ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', 'slider_assets', 100);
