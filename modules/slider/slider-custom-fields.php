<?php
add_action( 'cmb2_admin_init', 'slider_metabox' );

function slider_metabox() {

	$prefix = 'slider_';

	$slider = new_cmb2_box( [
		                        'id'           => $prefix . 'metabox',
		                        'title'        => esc_html__( 'Repeating Slide Group', 'cmb2' ),
		                        'object_types' => [ 'page', ],
		                        'show_on_cb'   => 'show_if_front_page'
	                        ] );

	$slider_id = $slider->add_field( [
		                                 'id'      => $prefix . 'slider',
		                                 'type'    => 'group',
		                                 'options' => [
			                                 'group_title'   => esc_html__( 'Slide {#}', 'cmb2' ),
			                                 'add_button'    => esc_html__( 'Add Another Slide', 'cmb2' ),
			                                 'remove_button' => esc_html__( 'Remove Slide', 'cmb2' ),
			                                 'sortable'      => true, // beta
		                                 ],
	                                 ] );

	$slider->add_group_field( $slider_id, [
		'name' => esc_html__( 'Slide Image', 'cmb2' ),
		'id'   => 'image',
		'type' => 'file',
	] );

}

add_action( 'cmb2_admin_init', 'slider_text_metabox' );


function slider_text_metabox() {

	$prefix = 'slider_';

	$cmb_demo = new_cmb2_box( [
		                          'id'           => $prefix . 'text_metabox',
		                          'title'        => esc_html__( 'Slider Text Metabox', 'cmb2' ),
		                          'object_types' => [ 'page', ],
		                          'show_on_cb'   => 'show_if_front_page'

	                          ] );

	$cmb_demo->add_field( [
		                      'name' => esc_html__( 'Slider Text', 'cmb2' ),
		                      'id'   => $prefix . 'text',
		                      'type' => 'textarea',

	                      ] );

}
