<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

// Add slider Shortcode
function slider_output() {
	
	ob_start();
	
	?>
	
	<div class="slider">
		
		<div class="slider-container">
			<?php
			
			$slides = get_post_meta( get_the_ID(), 'slider_slider', true );
			
			foreach ( $slides as $slide ) :
				
				echo '<div><img src="' . $slide['image'] . '"></div>';
			
			endforeach;
			
			?>
		</div>
	
	</div>
	
	<?php
	
	$output = ob_get_clean();
	
	return $output;
	
}

add_shortcode( 'slider', 'slider_output' );
