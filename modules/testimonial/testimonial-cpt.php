<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

function testimonial_init() {
	
	register_post_type( 'testimonial', [
		'labels'            => [
			'name'               => __( 'Testimonials', 'framework' ),
			'singular_name'      => __( 'Testimonial', 'framework' ),
			'all_items'          => __( 'All Testimonials', 'framework' ),
			'new_item'           => __( 'New Testimonial', 'framework' ),
			'add_new'            => __( 'Add New', 'framework' ),
			'add_new_item'       => __( 'Add New Testimonial', 'framework' ),
			'edit_item'          => __( 'Edit Testimonial', 'framework' ),
			'view_item'          => __( 'View Testimonial', 'framework' ),
			'search_items'       => __( 'Search Testimonials', 'framework' ),
			'not_found'          => __( 'No Testimonials found', 'framework' ),
			'not_found_in_trash' => __( 'No Testimonials found in trash', 'framework' ),
			'parent_item_colon'  => __( 'Parent Testimonial', 'framework' ),
			'menu_name'          => __( 'Testimonials', 'framework' ),
		],
		'public'            => false,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => [ 'title', 'editor' ],
		'has_archive'       => false,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-format-status',
		'show_in_rest'      => false,
	] );
	
}

add_action( 'init', 'testimonial_init' );

function testimonial_updated_messages( $messages ) {
	
	global $post;
	
	$permalink = get_permalink( $post );
	
	$messages['testimonial'] = [
		0  => '',
		1  => sprintf( __( 'Testimonial updated. <a target="_blank" href="%s">View Testimonial</a>', 'framework' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'framework' ),
		3  => __( 'Custom field deleted.', 'framework' ),
		4  => __( 'Testimonial updated.', 'framework' ),
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Testimonial restored to revision from %s', 'framework' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( __( 'Testimonial published. <a href="%s">View Testimonial</a>', 'framework' ), esc_url( $permalink ) ),
		7  => __( 'Testimonial saved.', 'framework' ),
		8  => sprintf( __( 'Testimonial submitted. <a target="_blank" href="%s">Preview Testimonial</a>', 'framework' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9  => sprintf( __( 'Testimonial scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Testimonial</a>', 'framework' ),
		               date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __( 'Testimonial draft updated. <a target="_blank" href="%s">Preview Testimonial</a>', 'framework' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];
	
	return $messages;
}

add_filter( 'post_updated_messages', 'testimonial_updated_messages' );

