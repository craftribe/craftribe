/*
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

/**
 * Init the testiomonials
 */
(function($) {
	$('.testimonial .testimonials').slick({
		infinite: true,
		dots: true,
		arrows: true,
		autoplay: true,
		autoplaySpeed: 5000,
		speed: 1000
	});
})(jQuery);