<?php
// Add testimonials Shortcode
function testimonials_output() {
	
	$args = [
		'post_type'   => [ 'testimonial' ],
		'post_status' => [ 'publish' ],
	];
	
	$testimonials = new WP_Query( $args );
	
	ob_start(); ?>
	
	<section class="testimonial content-section ">
		
		<div class="inner">
			
			<div class="container">
				
				<div class="section-title">
					<h2>What our <strong>Clients</strong> say</h2>
				</div>
				
				<div class="testimonials">
					
					<div>
						
						<p>Lorem ipsum dolor sit amet, at vis labores dolores atomorum, iudico euismod ad vel. Vim quis rebum definiebas et. No cetero tamquam expetendis has.</p>
						<span>-- Joe Bloggs</span>
					
					</div>
					
					<div>
						
						<p>Lorem ipsum dolor sit amet, at vis labores dolores atomorum, iudico euismod ad vel. Vim quis rebum definiebas et. No cetero tamquam expetendis has.</p>
						<span>-- Joe Bloggs</span>
					
					</div>
					
					<div>
						
						<p>Lorem ipsum dolor sit amet, at vis labores dolores atomorum, iudico euismod ad vel. Vim quis rebum definiebas et. No cetero tamquam expetendis has.</p>
						<span>-- Joe Bloggs</span>
					
					</div>
				
				</div>
			</div>
		</div>
	</section>
	
	<?php
	
	$output = ob_get_clean();
	
	return $output;
	
}

add_shortcode( 'testimonial', 'testimonials_output' );
