<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

/**
 * Setup the testimonial module
 */
function testimonial_setup() {

	$testimonial_includes = [
		'/modules/testimonial/testimonial-cpt.php',
		'/modules/testimonial/testimonial-shortcode.php'
	];

	foreach ( $testimonial_includes as $file ) {
		if ( ! $filepath = locate_template( $file ) ) {
			trigger_error( sprintf( __( 'Error locating %s for inclusion', 'framework' ), $file ), E_USER_ERROR );
		}

		require_once $filepath;
	}
	unset( $file, $filepath );

}

add_action( 'after_setup_theme', 'testimonial_setup' );

/**
 * testimonial module assets
 */
function testimonial_assets() {

	wp_enqueue_script( 'framework/testimonial', get_template_directory_uri() . '/modules/testimonial/assets/js/testimonial.js', [ 'jquery' ], null, true );
}

add_action( 'wp_enqueue_scripts', 'testimonial_assets', 100 );


