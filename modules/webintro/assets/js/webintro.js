/*
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

/**
 * Init the website intro
 */
(function($) {
	$('.intro-content').slick({
		vertical : true,
		verticalSwiping : true,
		infinite: true,
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		speed: 1000
	});
})(jQuery);