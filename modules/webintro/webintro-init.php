<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

$webintro_includes = [
	'/modules/webintro/webintro-shortcode.php'
];

foreach ($webintro_includes as $file) {
	if (!$filepath = locate_template($file)) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'framework'), $file), E_USER_ERROR);
	}
	
	require_once $filepath;
}
unset($file, $filepath);

/**
 * Module assets
 */
function webintro_assets() {

	wp_enqueue_script('framework/webintro', get_template_directory_uri().'/modules/webintro/assets/js/webintro.js', ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', 'webintro_assets', 100);
