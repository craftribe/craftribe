<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */
// Add slider Shortcode
function webintro_output() {
	
	ob_start();
	
	?>
	
	<section class="webintro">
		<div class="row">
			<div class="col-md-4">
				<h1 class="logo"><strong>Glas<span>space</span></strong>.</h1>
			</div>
			<div class="col-md-8">
				<h2><strong>Bespoke <span>Glass Rooms</span></strong> that bring the <span>outside</span>, <span>inside</span>.</h2>
			</div>
		</div>
		

		

		<div class="intro-content">

			<img id="intro_image" src="/wp-content/themes/glasspace/assets/images/large-home-slide.jpg" alt="Glasspace">
			<img id="intro_image" src="/wp-content/themes/glasspace/assets/images/large-home-slide.jpg" alt="Glasspace">
			<img id="intro_image" src="/wp-content/themes/glasspace/assets/images/large-home-slide.jpg" alt="Glasspace">

		</div>

		<a href="#wrap" class="mouse"></a>

	</section>
	
	<?php
	
	$output = ob_get_clean();
	
	return $output;
	
}

add_shortcode( 'webintro', 'webintro_output' );
