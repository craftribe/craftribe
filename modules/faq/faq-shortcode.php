<?php

// Add faqs Shortcode

function faqs_output() {

	$args = [
		'post_type'   => [ 'faq' ],
		'post_status' => [ 'publish' ],
	];

	$faqs = new WP_Query( $args );

	$faq_count = 0;

	ob_start();

	echo '<div id="faq-accordion" role="tablist" aria-multiselectable="true">';
	
	if ( $faqs->have_posts() ) :

		while( $faqs->have_posts() ) :
			$faq_count ++;
			$faqs->the_post();
			?>
			<div class="card">
				<div class="card-header" role="tab" id="heading<?= $faq_count; ?>">
					<h5 class="mb-0">
						<a data-toggle="collapse" data-parent="#faq-accordion" href="#collapse<?= $faq_count; ?>" aria-expanded="true" aria-controls="collapse<?= $faq_count; ?>">
							<?= get_the_title(); ?>
						</a>
					</h5>
				</div>

				<div id="collapse<?= $faq_count; ?>" class="collapse <?php if ( $faq_count == '1' ) : echo 'in'; endif; ?>" role="tabpanel" aria-labelledby="heading<?= $faq_count; ?>">
					<div class="card-block">
						<?= get_the_content(); ?>
					</div>
				</div>
			</div>

			<?php


		endwhile;

	else :

		echo 'No faqs';

	endif;
	
	wp_reset_postdata();
	
	echo '</div>';

	$output = ob_get_clean();

	return $output;
	
}

add_shortcode( 'faqs', 'faqs_output' );
