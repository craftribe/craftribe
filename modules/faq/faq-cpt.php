<?php

function faq_init() {
	register_post_type( 'faq', array(
		'labels'            => array(
			'name'                => __( 'FAQs', 'cmb' ),
			'singular_name'       => __( 'FAQ', 'cmb' ),
			'all_items'           => __( 'All FAQs', 'cmb' ),
			'new_item'            => __( 'New FAQ', 'cmb' ),
			'add_new'             => __( 'Add New', 'cmb' ),
			'add_new_item'        => __( 'Add New FAQ', 'cmb' ),
			'edit_item'           => __( 'Edit FAQ', 'cmb' ),
			'view_item'           => __( 'View FAQ', 'cmb' ),
			'search_items'        => __( 'Search FAQs', 'cmb' ),
			'not_found'           => __( 'No FAQs found', 'cmb' ),
			'not_found_in_trash'  => __( 'No FAQs found in trash', 'cmb' ),
			'parent_item_colon'   => __( 'Parent FAQ', 'cmb' ),
			'menu_name'           => __( 'FAQs', 'cmb' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-thumbs-up',
		'show_in_rest'      => true,
		'rest_base'         => 'faq',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );
	
}
add_action( 'init', 'faq_init' );

function faq_updated_messages( $messages ) {
	global $post;
	
	$permalink = get_permalink( $post );
	
	$messages['faq'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('FAQ updated. <a target="_blank" href="%s">View FAQ</a>', 'cmb'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'cmb'),
		3 => __('Custom field deleted.', 'cmb'),
		4 => __('FAQ updated.', 'cmb'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('FAQ restored to revision from %s', 'cmb'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('FAQ published. <a href="%s">View FAQ</a>', 'cmb'), esc_url( $permalink ) ),
		7 => __('FAQ saved.', 'cmb'),
		8 => sprintf( __('FAQ submitted. <a target="_blank" href="%s">Preview FAQ</a>', 'cmb'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('FAQ scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview FAQ</a>', 'cmb'),
			// translators: Publish box date format, see http://php.net/date
			          date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('FAQ draft updated. <a target="_blank" href="%s">Preview FAQ</a>', 'cmb'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);
	
	return $messages;
}
add_filter( 'post_updated_messages', 'faq_updated_messages' );