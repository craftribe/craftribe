<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

$faq_includes = [
	'/modules/faq/faq-cpt.php',
	'/modules/faq/faq-shortcode.php'
];

foreach ($faq_includes as $file) {
	if (!$filepath = locate_template($file)) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'framework'), $file), E_USER_ERROR);
	}
	
	require_once $filepath;
}
unset($file, $filepath);