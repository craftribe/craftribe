<?php

add_action( 'cmb2_admin_init', 'project_metabox' );

function project_metabox() {
	
	$prefix = 'project_';
	
	$project = new_cmb2_box( [
		'id'           => $prefix . 'metabox',
		'title'        => esc_html__( 'Project Info', 'framework' ),
		'object_types' => [ 'project', ],
	] );
	
	$project->add_field( [
		'name' => esc_html__( 'Project URL', 'framework' ),
		'id'   => $prefix . 'text',
		'type' => 'text_url',
	] );
	
	$project->add_field( [
		'name' => esc_html__( 'Project Main Image', 'framework' ),
		'id'   => $prefix . 'image',
		'type' => 'file',
	] );
}