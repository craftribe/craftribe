/*
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

(function($){

	$('.exterior-gallery').featherlightGallery({
		gallery   : {
			fadeIn : 300,
			fadeOut: 300
		},
		openSpeed : 300,
		closeSpeed: 300
	});

	$('.interior-gallery').featherlightGallery({
		gallery   : {
			fadeIn : 300,
			fadeOut: 300
		},
		openSpeed : 300,
		closeSpeed: 300
	});

})(jQuery);