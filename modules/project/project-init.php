<?php
/**
 * Setup the project module
 */
function project_setup() {
	
	// Add the archive featured image size
	add_image_size( 'project-thumbnail', 585, 355, true );
	add_image_size( 'project-single-header', 1920, 650, true );
	
	// Set up project includes
	$project_includes = [
		'/modules/project/project-cpt.php',
		'/modules/project/project-custom-fields.php',
		'/modules/project/project-single-featured-shortcode.php'
	];
	
	foreach ( $project_includes as $file ) {
		if ( ! $filepath = locate_template( $file ) ) {
			trigger_error( sprintf( __( 'Error locating %s for inclusion', 'framework' ), $file ), E_USER_ERROR );
		}
		
		require_once $filepath;
	}
	
	unset( $file, $filepath );
	
}

add_action( 'after_setup_theme', 'project_setup' );

/**
 * route project module archive template
 *
 * @param $template
 *
 * @return string
 */
function project_archive_template( $template ) {
	
	if ( is_post_type_archive( 'project' ) ) {
		$theme_files     = [ 'archive-project.php' ];
		$exists_in_theme = locate_template( $theme_files, false );
		if ( $exists_in_theme == '' ) {
			return get_template_directory() . '/modules/project/templates/archive-project.php';
		}
	}
	
	return $template;
	
}

add_filter( 'archive_template', 'project_archive_template' );

/**
 * route project module single template
 *
 * @param $single_template
 *
 * @return string
 */
function project_single_template( $single_template ) {
	
	global $post;
	
	if ( $post->post_type == 'project' ) {
		$single_template = get_template_directory() . '/modules/project/templates/single-project.php';
	}
	
	return $single_template;
}

add_filter( 'single_template', 'project_single_template' );

