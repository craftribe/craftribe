<?php

function project_init() {
	register_post_type( 'project', [
		'labels'                => [
			'name'               => __( 'Projects', 'cmb' ),
			'singular_name'      => __( 'Project', 'cmb' ),
			'all_items'          => __( 'All Projects', 'cmb' ),
			'new_item'           => __( 'New Project', 'cmb' ),
			'add_new'            => __( 'Add New', 'cmb' ),
			'add_new_item'       => __( 'Add New Project', 'cmb' ),
			'edit_item'          => __( 'Edit Project', 'cmb' ),
			'view_item'          => __( 'View Project', 'cmb' ),
			'search_items'       => __( 'Search Projects', 'cmb' ),
			'not_found'          => __( 'No Projects found', 'cmb' ),
			'not_found_in_trash' => __( 'No Projects found in trash', 'cmb' ),
			'parent_item_colon'  => __( 'Parent Project', 'cmb' ),
			'menu_name'          => __( 'Projects', 'cmb' ),
		],
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => [ 'title', 'editor', 'thumbnail', 'excerpt' ],
		'has_archive'           => true,
		'rewrite'               => [ 'slug' => 'portfolio' ],
		'query_var'             => true,
		'menu_icon'             => 'dashicons-thumbs-up',
		'show_in_rest'          => true,
		'rest_base'             => 'project',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	] );
	
}

add_action( 'init', 'project_init' );

function project_updated_messages( $messages ) {
	global $post;
	
	$permalink = get_permalink( $post );
	
	$messages['project'] = [
		0  => '', // Unused. Messages start at index 1.
		1  => sprintf( __( 'Project updated. <a target="_blank" href="%s">View Project</a>', 'cmb' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'cmb' ),
		3  => __( 'Custom field deleted.', 'cmb' ),
		4  => __( 'Project updated.', 'cmb' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Project restored to revision from %s', 'cmb' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( __( 'Project published. <a href="%s">View Project</a>', 'cmb' ), esc_url( $permalink ) ),
		7  => __( 'Project saved.', 'cmb' ),
		8  => sprintf( __( 'Project submitted. <a target="_blank" href="%s">Preview Project</a>', 'cmb' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9  => sprintf( __( 'Project scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Project</a>', 'cmb' ),
			// translators: Publish box date format, see http://php.net/date
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __( 'Project draft updated. <a target="_blank" href="%s">Preview Project</a>', 'cmb' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];
	
	return $messages;
}

add_filter( 'post_updated_messages', 'project_updated_messages' );


/**
 * Registers the `type` taxonomy,
 * for use with 'project'.
 */
function type_init() {
	register_taxonomy( 'type', [ 'project' ], [
		'hierarchical'          => false,
		'public'                => true,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_admin_column'     => false,
		'query_var'             => true,
		'rewrite'               => true,
		'capabilities'          => [
			'manage_terms' => 'edit_posts',
			'edit_terms'   => 'edit_posts',
			'delete_terms' => 'edit_posts',
			'assign_terms' => 'edit_posts',
		],
		'labels'                => [
			'name'                       => __( 'Types', 'framework' ),
			'singular_name'              => _x( 'Type', 'taxonomy general name', 'framework' ),
			'search_items'               => __( 'Search Types', 'framework' ),
			'popular_items'              => __( 'Popular Types', 'framework' ),
			'all_items'                  => __( 'All Types', 'framework' ),
			'parent_item'                => __( 'Parent Type', 'framework' ),
			'parent_item_colon'          => __( 'Parent Type:', 'framework' ),
			'edit_item'                  => __( 'Edit Type', 'framework' ),
			'update_item'                => __( 'Update Type', 'framework' ),
			'view_item'                  => __( 'View Type', 'framework' ),
			'add_new_item'               => __( 'New Type', 'framework' ),
			'new_item_name'              => __( 'New Type', 'framework' ),
			'separate_items_with_commas' => __( 'Separate types with commas', 'framework' ),
			'add_or_remove_items'        => __( 'Add or remove types', 'framework' ),
			'choose_from_most_used'      => __( 'Choose from the most used types', 'framework' ),
			'not_found'                  => __( 'No types found.', 'framework' ),
			'no_terms'                   => __( 'No types', 'framework' ),
			'menu_name'                  => __( 'Types', 'framework' ),
			'items_list_navigation'      => __( 'Types list navigation', 'framework' ),
			'items_list'                 => __( 'Types list', 'framework' ),
			'most_used'                  => _x( 'Most Used', 'type', 'framework' ),
			'back_to_items'              => __( '&larr; Back to Types', 'framework' ),
		],
		'show_in_rest'          => true,
		'rest_base'             => 'type',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	] );
	
}

add_action( 'init', 'type_init' );

/**
 * Sets the post updated messages for the `type` taxonomy.
 *
 * @param  array $messages Post updated messages.
 *
 * @return array Messages for the `type` taxonomy.
 */
function type_updated_messages( $messages ) {
	
	$messages['type'] = [
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Type added.', 'framework' ),
		2 => __( 'Type deleted.', 'framework' ),
		3 => __( 'Type updated.', 'framework' ),
		4 => __( 'Type not added.', 'framework' ),
		5 => __( 'Type not updated.', 'framework' ),
		6 => __( 'Types deleted.', 'framework' ),
	];
	
	return $messages;
}

add_filter( 'term_updated_messages', 'type_updated_messages' );
