<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

get_template_part( 'templates/page', 'header' );

if ( ! have_posts() ) : ?>
	
	<div class="alert alert-warning">
		<?php _e( 'Sorry, no results were found.', 'framework' ); ?>
	</div>
	<?php
	
	get_search_form();

endif;

$project_count = 1;

echo '<div class="row">';

while( have_posts() ) : the_post(); ?>
	
	<article <?php post_class( 'col-md-6' ); ?>>
		<a href="<?php the_permalink(); ?>">
			<?php
			if ( has_post_thumbnail() ) :
				the_post_thumbnail( 'project-thumbnail', [ 'class' => 'img-fluid' ] );
			endif;
			?>
			
			<header>
				<i class="project-count">0<?= $project_count; ?></i>
				<h4 class="project-title"><?php the_title(); ?></h4>
			</header>
		</a>
	</article>
	
	<?php
	
	if ( $project_count % 2 === 0 ) :
		
		echo '</div><div class="row">';
	
	endif;
	
	$project_count ++;

endwhile;

echo '</div>';

the_posts_navigation();
