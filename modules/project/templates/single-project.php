<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

while( have_posts() ) : the_post(); ?>
	<article <?php post_class(); ?>>
		<header>
			<?php
			if ( has_post_thumbnail() ) :
				the_post_thumbnail( 'project-single-header', [ 'class' => 'img-fluid' ] );
			endif;
			?>
			<h1 class="project-title"><?php the_title(); ?></h1>

		</header>

		<div class="project-content">

			<div class="project-description project-section">

				<div class="container">

					<h2>Project Description.</h2>

					<div class="row">

						<div class="col-md-4 project-info">

							<ul class="list-group">
								<li class="list-group-item">
									<strong>Client:</strong> Any Client
								</li>
								<li class="list-group-item">
									<strong>Completion Date:</strong> 01/01/16
								</li>
								<li class="list-group-item">
									<strong>Project Location:</strong> Winchester, UK
								</li>
								<li class="list-group-item">
									<strong>Products Used</strong><br/><span class="tag tag-default">Frameless Windows</span>
								</li>
								<li class="list-group-item">
									<strong>Services Provided</strong><br/><span class="tag tag-default">service</span>
									<span class="tag tag-default">service</span>
									<span class="tag tag-default">service</span>
								</li>
							</ul>

						</div>

						<div class="col-md-8 description">

							<p class="intro">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras placerat lobortis pellentesque. Praesent libero diam, porttitor nec justo ut, gravida sodales leo. </p>

							<?php the_content(); ?>

						</div>

					</div>

				</div>

			</div>

			<div class="project-gallery-exterior project-section alt">

				<div class="container">
					<h3>Project Exterior.</h3>

					<div class="row gallery-row">

						<div class="col-md-4">
							<a class="thumbnail exterior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/360x360" class="img-fluid"></a>
						</div>

						<div class="col-md-8">
							<a class="thumbnail exterior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/750x360" class="img-fluid"></a>
						</div>
					</div>

					<div class="row gallery-row">

						<div class="col-md-6">
							<a class="thumbnail exterior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/555x360" class="img-fluid"></a>
						</div>

						<div class="col-md-6">
							<a class="thumbnail exterior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/555x360" class="img-fluid"></a>
						</div>
					</div>

					<div class="row gallery-row">

						<div class="col-md-8">
							<a class="thumbnail exterior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/750x360" class="img-fluid"></a>
						</div>

						<div class="col-md-4">
							<a class="thumbnail exterior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/360x360" class="img-fluid"></a>
						</div>
					</div>
				</div>
			</div>

			<div class="project-gallery-interior project-section">

				<div class="container">
					<h3>Project Interior.</h3>
					
					<div class="row gallery-row">
						
						<div class="col-md-4">
							<a class="thumbnail interior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/360x360" class="img-fluid"></a>
						</div>
						
						<div class="col-md-8">
							<a class="thumbnail interior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/750x360" class="img-fluid"></a>
						</div>
					</div>
					
					<div class="row gallery-row">
						
						<div class="col-md-6">
							<a class="thumbnail interior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/555x360" class="img-fluid"></a>
						</div>
						
						<div class="col-md-6">
							<a class="thumbnail interior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/555x360" class="img-fluid"></a>
						</div>
					</div>
					
					<div class="row gallery-row">
						
						<div class="col-md-8">
							<a class="thumbnail interior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/750x360" class="img-fluid"></a>
						</div>
						
						<div class="col-md-4">
							<a class="thumbnail interior-gallery" href="<?php bloginfo( 'template_url' ); ?>/dist/images/GlassPace_10.jpg"><img src="http://placehold.it/360x360" class="img-fluid"></a>
						</div>
					</div>
				</div>

			</div>

			<div class="project-testimonial project-section alt">

				<div class="container">
					<h4>Project Testimonial.</h4>
				</div>
			</div>

		</div>

		</div>

	</article>
<?php endwhile; ?>
