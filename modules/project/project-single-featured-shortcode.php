<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

// Add project Shortcode
function project_single_featured_output() {
	
	ob_start();
	
	?>
	
	<section class="project-single-featured">
		
		<img class="visible-lg" src="<?php bloginfo( 'template_url' ); ?>/dist/images/large-pic.jpg" alt="Project" data-aos="fade-right">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-6">

					<div class="content" data-aos="fade-left">
						<div class="section-title text-md-left">
							<h3><strong>Featured</strong> Project</h3>
						</div>
						<h4>Project Title</h4>
						<p>Sed ut perspiciatis unde omnis iste natus laudantore veritatis et quasi architecto beatae vitae nunc sagittis interdum risusut accumsan. Donec faucibus mauris ullamcorper elit feugiat consectetur.</p>

						<div class="row">
							<div class="col-sm-6">
								<span class="lnr lnr-magnifier"></span>
								<h5>Project Overview</h5>
								<p>Sed ut perspiciatis unde lorems ipsum the natus errorm.</p>
							</div>

							<div class="col-sm-6">
								<span class="lnr lnr-diamond"></span>
								<h5>Project features</h5>
								<p>Sed ut perspiciatis unde lorems ipsum the natus errorm.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<?php
	
	// set output
	$output = ob_get_clean();
	
	// return output
	return $output;
	
}

add_shortcode( 'project_single_featured', 'project_single_featured_output' );
