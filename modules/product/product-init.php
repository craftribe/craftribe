<?php

$product_includes = [
	'/modules/product/product-cpt.php',
	'/modules/product/product-custom-fields.php',
	'/modules/product/product-shortcode.php'
];

foreach ($product_includes as $file) {
	if (!$filepath = locate_template($file)) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'framework'), $file), E_USER_ERROR);
	}
	
	require_once $filepath;
}
unset($file, $filepath);