<?php
/**
 * Copyright (c). 2016. WordPressGeek. All rights reserved.
 */

// Add product Shortcode
function product_output() {
	
	$count = 0;
	
	ob_start();
	
	?>
	
	<div class="product">

		<div class="product-container">
			<?php

			$slides = get_post_meta( get_the_ID(), 'product_product', true );

			foreach ( $slides as $slide ) :

				echo '<div><img src="' . $slide['image'] . '"></div>';

			endforeach;

			?>
		</div>

		<div class="overlay">
			<h2>Welcome to <?php bloginfo( 'name' ); ?></h2>
			<?= wpautop( get_post_meta( get_the_ID(), 'product_text', true ) ); ?>
		</div>

	</div>

	<?php

	// set output
	$output = ob_get_clean();
	
	// return output
	return $output;
	
}

add_shortcode( 'product', 'product_output' );
