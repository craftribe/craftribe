const Services = {

    init: function () {

        const self = this;

        self.servicesInit();

    },

    servicesInit: function () {

        const self = this;

        self.services.initServicesOverlay();

    },

    services: {

        initServicesOverlay: function () {

            $('.services__service__link').click(function (e) {

                e.preventDefault();

                const pane = $(this).parent('.services__service__inner').find('.services__service__overlay');

                $('html,body').addClass('no-scroll');
                $('.services').addClass('with-overlay');
                $(pane).addClass('services__service__overlay--active');



            });

            $('.services__service__overlay__close').click(function (e) {

                e.preventDefault();

                const pane = $(this).parent('.services__service__inner').find('.services__service__overlay');

                $('html,body').removeClass('no-scroll');
                $('.services').removeClass  ('with-overlay');
                $('.services__service__overlay').removeClass('services__service__overlay--active');

            });

        }
    }
};
