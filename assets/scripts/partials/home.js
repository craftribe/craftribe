const Home = {

    init: function () {

        const self = this;

        self.homeInit();

    },

    homeInit: function () {

        const self = this;

        self.home.initAnimation();

    },

    home: {

        initAnimation: function () {

            const line1 = $('.home__title span:nth-child(1)');
            const line2 = $('.home__title span:nth-child(3)');
            const line3 = $('.home__title span:nth-child(5)');
            const content = $('.home__content');
            const logoSolid = $('#craftribe-logo--solid');
            const logoOutline = $('#craftribe-logo--outline');

            const tl = new TimelineLite;

            tl
                .delay(2)


                .to(logoOutline, 0.05, {
                    css: {
                        opacity: '0'
                    },
                    ease: Back.easeInOut.config(0.7)

                })

                .from(logoSolid, 0.05, {
                    css: {
                        opacity: '0'
                    },
                    ease: Back.easeInOut.config(0.7)

                })


                .from(line1, 0.75, {
                    css: {
                        left: '-120%'
                    },
                    ease: Back.easeInOut.config(0.7)
                })

                .from(line2, 0.75, {
                    css: {
                        left: '-120%'
                    },
                    ease: Back.easeInOut.config(0.7)

                })

                .from(line3, 0.75, {
                    css: {
                        left: '-120%'
                    },
                    ease: Back.easeInOut.config(0.7)

                })

                .from(content, 0.75, {
                    css: {
                        opacity: '0'
                    },
                    ease: Back.easeInOut.config(0.7)

                });

        }
    }
};

