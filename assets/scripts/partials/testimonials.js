const Testimonials = {

    init: function () {

        const self = this;

        self.testimonialsInit();

    },

    testimonialsInit: function () {

        const self = this;

        self.testimonials.initSlider();

    },

    testimonials: {

        initSlider: function () {

            const testimonialSlider = $('.testimonials__slider');

            testimonialSlider.slick();

        }
    }
};

