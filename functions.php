<?php
/**
 * Framework includes
 */

$framework_includes = [
	
	// Scripts and stylesheets
	'lib/assets.php',
	// Custom functions
	'lib/extras.php',
	// Theme setup
	'lib/setup.php',
	// Page titles
	'lib/titles.php',
	// Theme wrapper class
	'lib/wrapper.php',
	// Theme customizer
	'lib/customizer.php',
	// Custom fields library
	'lib/cmb2/init.php',
	
	'modules/project/project-init.php'
		
];

foreach ( $framework_includes as $file ) {
	if ( ! $filepath = locate_template( $file ) ) {
		trigger_error( sprintf( __( 'Error locating %s for inclusion', 'framework' ), $file ), E_USER_ERROR );
	}

	require_once $filepath;
}
unset( $file, $filepath );



/**
 * @param $cmb
 *
 * @return bool
 */
function show_if_front_page( $cmb ) {
	
	// Don't show this metabox if it's not the front page template
	if ( $cmb->object_id !== get_option( 'page_on_front' ) ) {
		return false;
	}
	
	return true;
}