<!-- Home -->
<section id="home" class="home__slide">

    <span class="home__slide__image">
        <div></div>
    </span>
	
	<div class="home__slide__container">
		
		<h1 class="home__slide__title">
			<span>we are craftribe...</span>
			<br/>
			<span>we craft pixels.</span>
			<br/>
			<span>we give you time.</span>
		</h1>
		
		<p class="home__slide__content">In this day and age, time is precious so we focus our time on your digital
			presence to
			allow you to focus more time on your business.</p>
		<p class="home__slide__content">It really is that simple.</p>
		
		<p class="home__slide__content">
			<a href="#about" class="button button--purple">
				<span>about craftribe.</span>
			</a>
		</p>
	
	</div>

</section>
<!-- Home -->