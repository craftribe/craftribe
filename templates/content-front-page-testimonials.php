<section id="testimonials" class="testimonials">

	<span class="edge"> </span>

	<div class="testimonials__container">

		<h1>what people say about craftribe.</h1>

		<div class="testimonials__slider">

			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe are extremely skilled, knowledgable and professional. If you're looking for innovative web developers, I'd definitely recommend them. We have
						worked
						with craftribe for a while now on a variety of Wordpress, Ecommerce and bespoke projects and have had a first class service each and every time.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Christian, thinklab
					</div>

					<div class="testimonials__testimonial__company">
						PSD to HTML / PSD to WordPress
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe are absolute marvels at what they do and I could not of been more happy with the end product of my website. craftribe really go out of their way
						to
						communicate with you on every aspect of what you want while the development of the website is underway. I can not recommend craftribe enough. If you want
						something original and bespoke for your business, you will be well looked after and walk away with a website to be proud of.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Jade Wallis, JW Modelling School
					</div>

					<div class="testimonials__testimonial__company">
						Custom WordPress theme Design and Build // Branding Design
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">
				
				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe are fantastic. We cannot rate them highly enough. Very professional, very patient, very responsive and produced an amazing website for us which
						functions brilliantly on both normal PCs and mobile devices. We are so pleased and highly recommend them.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Louisa, Lulubaby.
					</div>

					<div class="testimonials__testimonial__company">
						Custom WordPress theme development
					</div>

				</div>

			</div>


			<div class="testimonials__testimonial">
				
				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe are excellent and I recommended them highly. They did a fantastic job and also gave very good practical advice and consulting. I will definitely
						be using them again
						and I will be passing their details on for others to use also.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Kate M, Motherprss
					</div>

					<div class="testimonials__testimonial__company">
						WordPress Amends // WordPress Maintenance // WordPress Consulting
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">
				
				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe are true web developer professionals. We are extremely pleased with the complicated site request that craftribe has built for us. They have been
						professional, timely and always at hand to fix any of ther minor issues as well as guiding us through the process. We will be using craftribe to further
						develop this site as well as for future developments.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Harry S, How to sell my business
					</div>

					<div class="testimonials__testimonial__company">
						Custom WordPress Theme Build // WordPress Consulting
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						Extremely prompt and helpful service. craftribe delivered high quality work. We will definitely work with them again in the future.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Jason V, Aeguana LTD
					</div>

					<div class="testimonials__testimonial__company">
						PSD to Custom WordPress Theme
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">
				

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe have been fantastic, they completed the project exactly as required and within a very tight timescale. I would definitely recommend craftribe to
						everyone and will be looking to use his services again.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						CJP
					</div>

					<div class="testimonials__testimonial__company">
						WordPress Fixes // WordPress maintenance
					</div>

				</div>

			</div>
			
			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe did a truly excellent job for me. I needed some PSD to HTML5 / CSS3 + jQuery conversions done under a very tight deadline, and they turned it
						around incredibly
						quickly. Communications throughout the project have been professional and friendly. craftribe have a 100% 'can-do' attitude and I look forward to working
						with them again soon.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Richard H
					</div>

					<div class="testimonials__testimonial__company">
						PSD to HTML 5/CSS3/jQuery
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						Great team, very good communication, understood brief and delivered it in record time. Would work with again.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Jamie, Intelligent Golf

					</div>

					<div class="testimonials__testimonial__company">
						Web Design // PSD to HTML 5/CSS3/jQuery

					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						It has been an absolute pleasure to work with craftribe. They work blindingly fast, they completed my project in 2 days with the other businesses needing at
						least 5. They resolved issues almost instantly and are continuing to give ongoing support. They are very professional and I feel privileged that I was able
						to work
						with them. I would not hesitate to recommend craftribe in the slightest!
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Kyle Clarke, Be something good
					</div>

					<div class="testimonials__testimonial__company">
						PSD to HTML 5/CSS3/jQuery // PSD to custom WordPress theme // WordPress Consultancy // WordPress Maintenance
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe were fast and efficient. They was clear with me every step of the process and the final result was made semantically.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Graeme G, Ampco Manufacturers Inc
					</div>

					<div class="testimonials__testimonial__company">
						WordPress development // WordPress Maintenance
					</div>

				</div>

			</div>


			<div class="testimonials__testimonial">
				
				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						This is the first time I worked with craftribe and I must say - Superb!! Work very fast and on point. I will be using craftribe's services again.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Melissa S, Healtheries
					</div>

					<div class="testimonials__testimonial__company">
						WordPress Development
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe did a brilliant job! Absolutely excellent. They did the work quickly and professionally and I look forward to working with them again soon!
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Jim P, Crisis Solutions
					</div>

					<div class="testimonials__testimonial__company">
						Custom WordPress Theme Development // Custom WordPress Plugin Development
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe are prompt and highly skilled workers. I'm very impressed with the quality of the communication, the timely nature of the work undertaken and the
						extremely professional quality given. They went above and beyond the service I requested and I am delighted with the results. I would highly recommend them.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Julian S, SM Technologies
					</div>

					<div class="testimonials__testimonial__company">
						PSD to WordPress // Custom WordPress Theme Development // WordPress Consulting
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">
				
				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						craftribe did an excellent job, they was quick, had great communications, and put in a lot of effort, Their expertise helped us out as well.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Ben H
					</div>

					<div class="testimonials__testimonial__company">
						Custom WordPress &amp; Development // WordPress Consulting
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">
				
				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						I would highly recommend craftribe as resourceful and proactive web developers on the WordPress platform. craftribe were quick to respond and what I
						appreciated most was their
						willingness to work with me via skype on lots of tweaks and enhancements. Two Thumbs up!
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Terry B
					</div>

					<div class="testimonials__testimonial__company">
						Custom WordPress Design &amp; Development // WordPress Consulting
					</div>

				</div>

			</div>


			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						Excellent! Work was completed within a few hours and craftribe went above and beyond to give me more than expected. Thank you
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						John B
					</div>

					<div class="testimonials__testimonial__company">
						WordPress Development
					</div>

				</div>

			</div>


			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						Really excellent quality of work. Completed the project really quickly and couldn't have been more helpful! I would definitely recommend.
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Lauren P
					</div>


					<div class="testimonials__testimonial__company">
						Bespoke WordPress Theme Design and Development
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">

				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						Absolutely first class job. Very quick, very professional. A*
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Gary, GB Press Releases.
					</div>

					<div class="testimonials__testimonial__company">
						WordPress Development
					</div>

				</div>

			</div>

			<div class="testimonials__testimonial">
				
				<div class="testimonials__testimonial__text__wrap">

					<div class="testimonials__testimonial__text">

						<span>&ldquo;</span>
						Super work and extremely quick, highly recommend craftribe and will use them again
						<span>&rdquo;</span>

					</div>

					<div class="testimonials__testimonial__author">
						Scott M
					</div>

					<div class="testimonials__testimonial__company">
						WordPress Development
					</div>


				</div>

			</div>


		</div>

		<p class="text-center">
			<a href="#portfolio" class="button">
				<span>portfolio.</span>
			</a>
		</p>

	</div>

</section>
