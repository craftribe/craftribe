<section id="portfolio" class="portfolio">

	<span class="edge"> </span>

	<div class="portfolio__container">

		<h1>craftribe's portfolio.</h1>

		<p class="portfolio__lead">
			craftribe have been lucky enough to work with many clients over the years. These include startups, small business, top brands and global agencies. Please take a
			look at a small selection of our work below, a full list of work undertaken by us is available upon request*.
		</p>

		<div class="portfolio__grid__layout">

			<svg class="hidden">
				<symbol id="icon-arrow" viewBox="0 0 24 24">
					<title>arrow</title>
					<polygon points="6.3,12.8 20.9,12.8 20.9,11.2 6.3,11.2 10.2,7.2 9,6 3.1,12 9,18 10.2,16.8 "/>
				</symbol>
				<symbol id="icon-cross" viewBox="0 0 24 23">
					<title>cross</title>
					<path
						d="M23.865 3.677c.197-.383.164-.818-.008-1.18.048-.41-.06-.827-.448-1.147L22.323.457c-.636-.524-1.632-.689-2.25 0a155.348 155.348 0 0 1-8.797 9.001C9.011 7.342 6.72 5.255 4.443 3.165c-.8-.734-1.956-.503-2.458.37C1.253 3.9.659 4.374.168 5.182c-.233.386-.215.872 0 1.258 1.47 2.635 4.31 4.951 6.646 7.083-.313.28-.623.562-.942.836-3.146 2.702-5.268 4.416-1.331 7.187.053.037.107.029.161.05.076.036.148.06.232.074.026 0 .05.005.075.003.082.007.16.027.243.011 2.117-.415 4.085-2.074 5.872-3.9 1.74 1.715 3.592 3.353 5.63 4.325.485.232 1.063.097 1.436-.227.626.047 1.197-.342 1.484-.901.042-.026.07-.041.116-.07.91-.569.993-1.701.32-2.482-1.522-1.762-3.138-3.438-4.787-5.084 2.968-2.9 6.674-6.027 8.542-9.667z"/>
				</symbol>
			</svg>

			<div class="grid">
				
				<?php
				
				$args = [
					'post_type'      => [ 'project' ],
					'post_status'    => [ 'publish' ],
					'posts_per_page' => - 1
				];
				
				$project_items = new WP_Query( $args );
				
				while ( $project_items->have_posts() ) : $project_items->the_post();
					
					$terms = get_the_terms( get_the_ID(), 'type' );
					
					$all_types = '';
					
					if ( $terms && ! is_wp_error( $terms ) ) :
						
						$types = [];
						
						foreach ( $terms as $term ) {
							$types[] = $term->name;
						}
						
						$all_types = join( ", ", $types );
					
					endif; ?>

					<a class="grid__item" href="#<?= get_post_field( 'post_name', get_post() ); ?>">
						<div class="box">
							<div class="box__shadow"></div>
							<img class="box__img" src="<?= get_the_post_thumbnail_url(); ?>" alt="<?= get_the_title(); ?>"/>
							<h3 class="box__title">
								<span class="box__title-inner box__title-inner-design" data-hover="<?= get_the_title(); ?>"><?= get_the_title(); ?></span>
							</h3>
							<h4 class="box__text box__text--bottom">
								<span class="box__text-inner"><?= esc_html( $all_types ); ?></span>
							</h4>
							<div class="box__deco">&#8903;</div>
						</div>
					</a>
					<?php
				
				endwhile;
				
				wp_reset_postdata();
				
				?>

			</div>

		</div>

		<div class="overlay">

			<div class="overlay__reveal"></div>
			
			<?php
			
			$args = [
				'post_type'      => [ 'project' ],
				'post_status'    => [ 'publish' ],
				'posts_per_page' => - 1
			];
			
			$project_items = new WP_Query( $args );
			
			while ( $project_items->have_posts() ) : $project_items->the_post();
				
				$terms = get_the_terms( get_the_ID(), 'type' );
				
				$all_types = '';
				
				if ( $terms && ! is_wp_error( $terms ) ) :
					
					$types = [];
					
					foreach ( $terms as $term ) {
						$types[] = $term->name;
					}
					
					$all_types = join( ", ", $types );
				
				endif; ?>

				<div class="overlay__item" id="<?= get_post_field( 'post_name', get_post() ); ?>">
					<div class="box">
						<div class="box__shadow"></div>
						<!--<img class="box__img box__img--original" src="<?= get_the_post_thumbnail_url(); ?>" alt="<?= get_the_title(); ?>"/>-->
						<img class="box__img box__img--original" src="<?= get_post_meta( get_the_ID(), 'project_image', true ); ?>"/>
						<h3 class="box__title">
							<span class="box__title-inner"><?= get_the_title(); ?></span>
						</h3>
						<div class="box__text">
							<div class="box__text-inner"> <?= esc_html( $all_types ); ?></div>
						</div>
						<div class="box__deco">&#8903;</div>
					</div>
					<div class="overlay__content scroll-item">
						<?= wpautop( get_the_content() ); ?>
						<?php
						if ( get_post_meta( get_the_ID(), 'project_text', true ) ) : ?>
							<p><a rel="nofollow" target="_blank" href="<?= get_post_meta( get_the_ID(), 'project_text', true ); ?>" class="btn btn-primary">Visit Site</a></p>
						
						<?php endif; ?>

					</div>

				</div>
				
				<?php
			
			endwhile;
			
			wp_reset_postdata();
			
			?>

			<button class="overlay__close">
				<i class="ti-close"></i>
			</button>

		</div>

		<p class="text-center">
			<small>*We are unable to display some of our portfolio due to
				providing whitelabel work to digital agencies across the globe.
			</small>
			<br/><br/><br/>
			<a href="#contact" class="button button--purple">
				<span>contact craftribe.</span>
			</a>
		</p>

	</div>

</section>