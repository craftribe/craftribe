<?php

add_action( 'cmb2_admin_init', 'portfolio_metabox' );
function portfolio_metabox() {
$prefix = 'portfolio_';
$portfolio = new_cmb2_box( [
	'id'           => $prefix . 'metabox',
	'title'        => esc_html__( 'portfolio Metabox', 'cmb2' )echo	] );
}
