<article <?php post_class(); ?>>
    <?php
    if (has_post_thumbnail()) : the_post_thumbnail(); endif;
    ?>
    <header>
        <h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <p class="date">
            <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
        </p>
    </header>
    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div>
</article>