<footer class="mastfooter" id="mastfooter">

	<span class="bubble-1 bubble"></span>
	<span class="bubble-2 bubble"></span>

	<span class="edge"> </span>

	<div class="contact" id="contact">

		<div class="container">

			<h1 class="text-center">
				<i class="ti-announcement"></i>
				contact craftribe.
			</h1>

			<div class="contact-details">

				<a href="#" class="contact-email">
					<i class="ti-email"></i>
					<span>click to send us a message.<br/><small>or email us on craftribe.digital@gmail.com</small></span>
				</a>

				<span class="contact-phone" href="tel:+447922807663">
                    <i class="ti-mobile"></i>
                    <span>click to give us a call.<br/><small>or call us on (07922) 807663</small></span>
                </span>

			</div>

			<form id="contact-form" action="index.php">

				<div>
					<h3>About you.</h3>
					<section>
						<div class="row">
							<div class="col-md-6">
								<label for="name">Name</label>
								<input id="name" name="name" placeholder="full name..." type="text">
							</div>
							<div class="col-md-6">
								<label for="email">Email</label>
								<input id="email" name="email" placeholder="email address..." type="text">
							</div>
							<div class="col-md-4">
								<label for="phone">Phone</label>
								<input id="phone" name="phone" placeholder="phone number..." type="text">
							</div>
							<div class="col-md-8">
								<label for="phone">URL</label>
								<input id="url" name="url" placeholder="existing website url (if applicable)..." type="text">
							</div>
						</div>

					</section>
					<h3>About your project.</h3>
					<section>
						<div class="row">
							<div class="col-md-12">
								<label for="name">About your project</label>
								<textarea id="message" name="message" placeholder="please write a brief description about your requirements..."></textarea>
							</div>

						</div>
					</section>
					<h3>Finish.</h3>
					<section>
					</section>
				</div>
			</form>

		</div>

	</div>


</footer>

<ul class="footer-actions">
	<li class="footer-action footer-action--to-top">
		<span class="info">
			scroll to top
		</span>

		<a href="#top" class="scroll-top">
			<span class="ti-angle-up"></span>
		</a>

	</li>
	<li class="footer-action footer-action--facebook">
		<div class="fb-like"
		     data-href="https://www.facebook.com/craftribe.digital/"
		     data-layout="box_count"
		     data-action="like"
		     data-size="small"
		     data-show-faces="false"
		     data-share="true">

		</div>
		<span class="info">
			Like us on facebook!
		</span>
	</li>
	<li class="footer-action footer-action--skype">
		<span class="skype-button bubble " data-contact-id="live:craftribe.digital" data-color="#005C97"></span>
		<script src="https://swc.cdn.skype.com/sdk/v1/sdk.min.js"></script>
		<span class="info">
			message us on skype
		</span>
	</li>
</ul>


<div id="map-canvas"></div>

