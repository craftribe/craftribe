<section id="about" class="about">

	<span class="edge"> </span>

	<span class="about__image"><div></div></span>

	<div class="about__container">

		<div class="about__row">

			<div class="about__content col-md-push-1 col-md-5">

				<h1>
					about craftribe.
				</h1>
				
				<?= wpautop(get_the_content());?>
				
				<p>
					<a href="#mastfooter">Get in touch today</a>
					with your requirements to find out how we can help you and your business thrive online.
				</p>
				<p>
					<a href="#services" class="button">
						<span>services.</span>
					</a>
				</p>

			</div>

		</div>

	</div>

</section>