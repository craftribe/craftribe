<section id="services" class="services">

	<span class="edge"> </span>

	<div class="services__container">

		<h1>craftribe do.</h1>

		<p class="services__lead">craftribe provide digital media services to many clients globally. craftribe create clean, modern designs for both web and print and we develop
			handcrafted, semantic code from the ground up.</p>
		<p class="services__lead">Find out more about what servcies craftribe offer below.</p>

		<div class="services__grid-container">

			<div class="services__service services__service--first">

				<div class="box__shadow"></div>

				<div class="services__service__inner">

					<i class="services__service__icon services__service--first__icon"></i>

					<h3>design.</h3>

					<p>craftribe design for web, print and digital display. craftribe offer all types of design services including branding,
						stationary, advertising and just about anything related to graphic design.</p>

					<a href="#" class="services__service__link services__service--first__link">
						read more
						<span class="ti-angle-right"></span>
					</a>

					<!-- Service Overlay -->
					<div class="services__service__overlay services__service--first__overlay">

						<a href="#" class="services__service__overlay__close">

							<i class="services__service__overlay__close__icon"></i>

						</a>

						<h2>craftribe design.</h2>

						<p>craftribe are creatives and love to create websites, branding, advertising, product packaging and more. craftribe work using modern
							design techniques, practices &amp;amp; patterns ensuring that you are provided with the unique yet affordable designs that reflect your brands digital
							image in the 21st century.</p>

						<div class="services__service__overlay__content">

							<div class="row">

								<div class="col-md-6">
									<div class="services__service__overlay__content__inner">
										<h3>let craftribe craft your identity.</h3>
										<p>craftribe offer their creativity to provide full fresh, modern branding and
											digital identity services for your start-up or business rebrand. This includes; logo
											design, business card design, letterhead design and more.</p>
										<p>craftribe have years of experience in designing for print and utilise that graphic design experience to meet any branding design
											requirements.</p>
									</div>
								</div>

								<div class="col-md-6">
									<img src="<?= get_template_directory_uri(); ?>/dist/images/portfolio/ctf-identity.jpg"
									     class="img-fluid" alt="CTF Full Identity Design">
									<br/><br/>
									<p class="text-center">
										<small>Full Identity Design for Creating The Future.</small>
									</p>


								</div>

							</div>

							<hr/>

							<div class="row alt">

								<div class="col-md-6">
									<img src="<?= get_template_directory_uri(); ?>/dist/images/portfolio/collins-nets-woocommerce-design.jpg"
									     class="img-fluid" alt="Custom WooCommerce WordPress website design and development for Collins Nets">
									<br/><br/>
									<p class="text-center">
										<small>Custom WooCommerce WordPress website design and development for Collins Nets.</small>
									</p>


								</div>

								<div class="col-md-6">
									<div class="services__service__overlay__content__inner">
										<h3>let craftribe craft your website.</h3>
										<p>craftribe offer full web design services using industry standard software, modern design techniques and principals to create a new, or
											update your current digital presence. We design small brochure style
											websites right through to full scale websites that may be eCommerce, blog or corporate etc. We
											can take full control of UX planning, UI design and also provide responsive design as standard to ensure
											your website will look great on all display mediums be it mobile, tablet or desktop.</p>
									</div>
								</div>

							</div>
							<hr/>
							<div class="row">

								<div class="col-md-6">
									<div class="services__service__overlay__content__inner">
										<h3>let us craftribe your social media presence.</h3>
										<p>craftribe offer social media design/setup as well as advertising design for you and your
											business. We can setup your social media presence, update all digital media such as logo's/artwork on all of your social media
											platforms, or we can provide all media in all of the correct sizes for you to
											upload yourself.</p>
									</div>
								</div>

								<div class="col-md-6">
									<style>
										video {
											width: 100% !important;
											height: auto !important;
										}
									</style>
									<video src="<?= get_template_directory_uri(); ?>/ben-facebook-animation.webm" controls>
									</video>
									<br/>
									<p class="text-center">
										<small>Facebook cover video design for BM Man and Van Services.</small>
									</p>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="services__service services__service--second">

				<div class="box__shadow"></div>

				<div class="services__service__inner">

					<i class="services__service__icon services__service--second__icon"></i>

					<h3>development.</h3>

					<p>craftribe develop for the web and are front end developers and backend developers who will bring
						your brief to life with handcrafted code utilising the latest technologies, techniques and trends. </p>


					<a href="#" class="services__service__link services__service--second__link">read more
						<span class="ti-angle-right"></span>
					</a>

					<!-- Service Overlay -->
					<div class="services__service__overlay services__service--second__overlay">

						<a href="#" class="services__service__overlay__close">

							<i class="services__service__overlay__close__icon"></i>

						</a>

						<h2>craftribe develop.</h2>

						<p>craftribe handcraft brochure websites, wordpress websites, ecommerce websites, portals and more from the ground up. craftribe also offer
							eCommerce solutions developing bespoke WooCommerce enabled themes for WordPress. craftribe handcraft clean, semantic, search engine optimised code bases
							to ensure
							bullet-proof, future-proof extensible solutions that quite simply, work for you and your
							business.</p>

						<div class="services__service__overlay__content text-center">

							<div class="row">

								<div class="col-md-6">
									<div class="services__service__overlay__content__inner">
										<h3>Static websites.</h3>
										<p>craftribe code up designs that were designed by us as well as provide PSD to
											HTML/CSS3/jQuery/PHP services. We also offer whitelabel services to Agencies looking to ease the workload. Before a website is released,
											it is speed tested,
											optimised, tested on mobile devices and spell checked, all to ensure the smooth release of your website and ongoing stability.</p>
									</div>
								</div>

								<div class="col-md-6">

									<div class="services__service__overlay__content__inner">
										<h3>Bespoke WordPress theme development.</h3>
										<p>Here at craftribe, we do not use off-the-shelf themes for websites. All of our
											<a
												href="https://en-gb.wordpress.org/" target="_blank">WordPress</a> work
											is bespoke and handcrafted from the ground up so we can ensure fast, streamlined, optimised experiences for your
											clients. craftribe provide WordPress Design and Development to individuals and small/medium businesses
											throughout the world.</p>
										<p>craftribe love to design and develop completely bespoke, custom WordPress websites from
											the ground up, from initial concept right through to a structured launch, craftribe provide completely
											customised WordPress design and development packages to suit your
											requirements.</p>
									</div>

								</div>

							</div>
							
							<hr/>

							<div class="row alt">

								<div class="col-md-6">
									<div class="services__service__overlay__content__inner">
										<h3>eCommerce websites.</h3>
										<p>Unless asked otherwise, craftribe use <a href="https://woocommerce.com/"
										                                            target="_blank">WooCommerce</a> to power
											your eCommerce online shop using custom coded, woocommerce enabled WordPress themes, handcoded from scratch. We
											can
											either develop using your design (PSD to Wordpress) or craftribe can provide the whole service, from planning
											to
											design to code to rollout.</p>
										<p>craftribe really can provide a fully tailored e-commerce design and code package to suit
											your requirements.</p>
									</div>
								</div>

								<div class="col-md-6">
									<div class="services__service__overlay__content__inner">
										<h3>agency services.</h3>
										<p>craftribe have worked with numerous global digital agencies providing PSD to WordPress
											services. craftribe code completely bespoke WordPress themes from the ground up using
											build
											tools such as Gulp and ScSS to streamline our work flow. craftribe also use GIT/SVN on
											all
											of their projects. craftribe do not like to use existing, 'off the shelf'
											themes/templates
											for WordPress like many other "WordPress Development Companies" out there. craftribe's
											work
											is bespoke and tailored to your needs utilising custom fields and custom post
											types
											to ensure maximum editibility whilst ensuring the initial design/layout is
											maintained.</p>
									</div>
								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="services__service services__service--third">

				<div class="box__shadow"></div>

				<div class="services__service__inner">

					<i class="services__service__icon services__service--third__icon"></i>

					<h3>consulting.</h3>

					<p>craftribe offer expert consulting on digital strategies for your business. craftribe can offer advice and give
						solutions to startups and
						small businesses who require advice from experts.</p>

					<a href="#" class="services__service__link services__service--third__link">
						read more
						<span class="ti-angle-right"></span>
					</a>

					<!-- Service Overlay -->
					<div class="services__service__overlay services__service--third__overlay">

						<a href="#" class="services__service__overlay__close">

							<i class="services__service__overlay__close__icon"></i>

						</a>

						<h2>craftribe consult.</h2>

						<p>At craftribe's core, there are years of experience in many sectors relating to digital media. We
							offer consulting services to those needing expert advice on graphic design, web development,
							social
							media, seo and more.</p>

						<div class="services__service__overlay__content">

							<div class="row">

								<div class="col-md-12 text-center">
									<div class="services__service__overlay__content__inner">

										<p>we offer:</p>
										<h3>design strategy consulting.</h3>
										<h3>development strategy consulting.</h3>
										<h3>wordpress development consulting.</h3>
										<h3>search engine optimisation strategy consulting.</h3>
										<h3>social media strategy consulting.</h3>

									</div>
								</div>


							</div>


						</div>

					</div>

				</div>

			</div>

		</div>

		<p class="text-center">
			<a href="#testimonials" class="button button--purple">
				<span>testimonials.</span>
			</a>
		</p>

	</div>

</section>