<!-- home -->
<?php get_template_part( 'templates/content', 'front-page-home' ); ?>
<!-- home -->

<!-- about -->
<?php get_template_part( 'templates/content', 'front-page-about' ); ?>
<!-- about -->

<!-- services -->
<?php get_template_part( 'templates/content', 'front-page-services' ); ?>
<!-- services -->

<!-- testimonials -->
<?php get_template_part( 'templates/content', 'front-page-testimonials' ); ?>
<!-- testimonials -->

<!-- portfolio -->
<?php get_template_part( 'templates/content', 'front-page-portfolio' ); ?>
<!-- portfolio -->